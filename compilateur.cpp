//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

char current;				// Current car
char nextcar;

void ReadChar(void){		// Read character and skip spaces until
				// non space character is read
				current = nextcar;
	while(cin.get(nextcar) && (nextcar==' '||nextcar=='\t'||nextcar=='\n'))
	{
	}
}

/*void ReadChar(void){
  if(NLookedAhead>0){
    current=lookedAhead;    // Char has already been read
    NLookedAhead--;
  }
  else
    // Read character and skip spaces until
    // non space character is read
    while(cin.get(current) && (current==' '||current=='\t'||current=='\n'));
}

void LookAhead(void){
    while(cin.get(lookedAhead) && (lookedAhead==' '||lookedAhead=='\t'||lookedAhead=='\n'));
    NLookedAhead++;
}*/

void Error(string s){
	cerr<< s << endl;
	exit(-1);
}

// ArithmeticExpression := Term {AdditiveOperator Term}
// Term := Digit | "(" ArithmeticExpression ")"
// AdditiveOperator := "+" | "-"
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// OperateurRelationnel := "="|"<>"|"<"|"<="|">="|">"
// Expression := ArithmeticExpression | ArithmeticExpression OperateurRelationnel ArithmeticExpression


void AdditiveOperator(void){
	if(current=='+'||current=='-')
		ReadChar();
	else
		Error("Opérateur additif attendu");	   // Additive operator expected
}

void Digit(void){
	if((current<'0')||(current>'9'))
		Error("Chiffre attendu");		   // Digit expected
	else{
		cout << "\tpush $"<<current<<endl;
		ReadChar();
	}
}

void ArithmeticExpression(void);			// Called by Term() and calls Term()

void Term(void){
	if(current=='('){
		ReadChar();
		ArithmeticExpression();
		if(current!=')')
			Error("')' était attendu");		// ")" expected
		else
			ReadChar();
	}
	else
		if (current>='0' && current <='9')
			Digit();
	     	else
			Error("'(' ou chiffre attendu");
}

void ArithmeticExpression(void){
	char adop;
	Term();
	while(current=='+'||current=='-'){
		adop=current;		// Save operator in local variable
		AdditiveOperator();
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		if(adop=='+')
			cout << "\taddq	%rbx, %rax"<<endl;	// add both operands
		else
			cout << "\tsubq	%rbx, %rax"<<endl;	// substract both operands
		cout << "\tpush %rax"<<endl;			// store result
	}
}

void OperateurRelationnel(void);

void Expression(void){
	enum OPREL {EQUAL, DIFF, INF, INFE, SUP, SUPE};
	OPREL Oprel;
	ArithmeticExpression();
	if(current=='='||current=='<'||current=='>'){
		if(current == '='){
			Oprel = EQUAL;
		}
		else if(current == '<'){
			if(nextcar == '='){
				Oprel = INFE;
			}
			else if(nextcar == '>'){
				Oprel = DIFF;
			}
			else{
				Oprel = INF;
			}
		}
		else if(current == '>'){
			if(nextcar == '='){
				Oprel = SUPE;
			}
			else{
				Oprel = SUP;
			}
		}
		OperateurRelationnel();
		ArithmeticExpression();
		cout << "\tpop %rbx"<<endl;
		cout << "\tpop %rax"<<endl;
		cout << "\tcmpq	%rbx, %rax"<<endl;
		switch (Oprel){
			case EQUAL:
				cout<< "\tje Vrai"<<endl;
				cout<<"\tjne Faux"<<endl;
			case DIFF:
				cout<< "\tjne Vrai"<<endl;
				cout<< "\tje Faux"<<endl;
			case INF:
				cout<< "\tjb Vrai"<<endl;
				cout<< "\tjae Faux"<<endl;
			case INFE:
				cout<< "\tjbe Vrai"<<endl;
				cout<< "\tja Faux"<<endl;
			case SUPE:
				cout<< "\tjae Vrai"<<endl;
				cout<< "\tjb Faux"<<endl;
			case SUP:
				cout<< "\tja Vrai"<<endl;
				cout<< "\tjbe Faux"<<endl;
		}
		cout <<"Faux :"<<"\tpush $0"<<endl;
		cout <<"Vrai :"<<"\tpush $0xFFFFFFFFFFFFFFFF"<<endl;
		cout <<"\tjmp Fin"<<endl;
		cout<<"Fin :"<<endl;
	}
	else{
		cout<<"Operateur relationnel attendu"<<endl;
	}
}

void OperateurRelationnel(void){
	if(current=='<'){
		if(nextcar=='>'||nextcar=='='){
			ReadChar();
		}
		ReadChar();
	}
	else if(current=='>'){
		if(nextcar=='='){
			ReadChar();
		}
		ReadChar();
	}
	else if(current=='='){
		ReadChar();
	}
	else{
		Error("Operateur relationnel attendu");
	}
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;

	// Let's proceed to the analysis and code production
	ReadChar();
	ReadChar();
	Expression();
	ReadChar();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(cin.get(current)){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
